package ua.dp.rundot;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.UUID;

/**
 * Created by emaksimovich on 03.02.17.
 */
public class Client {

    private final static int PORT = 4444;
    private final static String CHAT_GROUP = "228.1.1.1";
    private String userId;
    private MulticastSocket socket;

    public Client() {
        try {
            socket = new MulticastSocket(PORT);
            InetAddress groupAddress = InetAddress.getByName(CHAT_GROUP);
            socket.joinGroup(groupAddress);
            userId = UUID.randomUUID().toString();
            new SocketListener();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class SocketListener extends Thread {

        public SocketListener() {
            start();
        }

        @Override
        public void run() {
            byte[] buffer = new byte[2048];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            try {
                while (true) {
                    socket.receive(packet);
                    try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(packet.getData()))){
                        Message message = (Message) in.readObject();
                        if (!message.getUserId().equals(userId))
                            System.out.println(packet.getAddress() + ":" + message.getText());
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void chat() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try (
            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(byteOut);)
            {
            while (true) {
                String str = reader.readLine();
                Message message = new Message(userId, str);
                out.writeObject(message);
                byte[] buffer = byteOut.toByteArray();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(CHAT_GROUP), PORT);
                socket.send(packet);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.chat();
    }

}
